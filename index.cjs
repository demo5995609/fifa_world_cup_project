const path = require("path");
const csv = require("csvtojson");
const fs = require("fs");

// File Paths
const WorldCupMatchesPath = path.join(
  __dirname,
  "./src/data/WorldCupMatches.csv"
);
const WorldCupsPath = path.join(__dirname, "./src/data/WorldCups.csv");
const WorldCupPlayersPath = path.join(
  __dirname,
  "./src/data/WorldCupPlayers.csv"
);

//      *****************Geting data of Find Matches by Per Cities*************

csv()
  .fromFile(WorldCupMatchesPath)
  .then((worldMatches) => {
    let findMatchesByPerCities = require("./src/server/findMatchCities.cjs");
    let result = findMatchesByPerCities(worldMatches);
    fs.writeFileSync(
      path.join(__dirname, "./src/public/output/matchesPerCities.json"),
      JSON.stringify(result),
      "utf-8"
    );
  });

//      *************Matchces Won Per Team function calling*************

csv()
  .fromFile(WorldCupMatchesPath)
  .then((worldCupMatches) => {
    let matchesWonPerTeams = require("./src/server/matchesPerTeam.cjs");

    // MatchesWonPerTeams calling
    let result = matchesWonPerTeams(worldCupMatches);
    fs.writeFileSync(
      path.join(__dirname, "./src/public/output/matchesWonPerTeams.json"),
      JSON.stringify(result),
      "utf-8"
    );
  });

//          *****************Find the number of red Cards issued per teams in 2014**********

csv()
  .fromFile(WorldCupMatchesPath)
  .then((jsonObjMatches) => {
    csv()
      .fromFile(WorldCupPlayersPath)
      .then((jsonObjPlayer) => {
        let findRedCardsPerTeams = require("./src/server/findRedCardsPerTeams.cjs");
        // findRedCardsPerTeams function accessing and calling
        let finalRedTeams = findRedCardsPerTeams(jsonObjMatches, jsonObjPlayer);
        fs.writeFileSync(
          path.join(__dirname, "./src/public/output/redTeamsCounting2014.json"),
          JSON.stringify(finalRedTeams),
          "utf-8"
        );
      });
  });

//              ***************Find the top 10 Probabitilty players***************

csv()
  .fromFile(WorldCupPlayersPath)
  .then((jsonObjPlayer) => {
    // top10Players function accessing and calling
    let top10Players = require("./src/server/top10Players.cjs");
    let topplayers = top10Players(jsonObjPlayer);
    fs.writeFileSync(
      path.join(__dirname, "./src/public/output/top10PlayersList.json"),
      JSON.stringify(topplayers),
      "utf-8"
    );
  });
