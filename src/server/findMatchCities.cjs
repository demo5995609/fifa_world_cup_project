// Find Matches by cities function creation
function findMatchesByPerCities(matches) {
    let result = matches.filter((matche) => matche.City !== '').reduce((acc, match) =>{
        if ( acc[match.City] ) {
            acc[match.City] += 1;
        } else {
            acc[match.City] = 1;
        }
        return acc;
    },{});
    // console.log(result)
    return result;
}


module.exports = findMatchesByPerCities;