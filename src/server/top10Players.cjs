function top10Players(jsonObjPlayer) {
  //iterating whole jsonObjPlayer
  let playersListScores = jsonObjPlayer
    .filter((playerObj) => playerObj.Event !== "")
    .reduce((acc, player) => {
      // Making Array of json data
      let arrOfEvents = player.Event.split(" ");
      let goalsCount = 0;

      if (arrOfEvents[0].charAt(0) == "G") {
        goalsCount = 1;
      }

      //Adding the Goals count or declaring
      if (acc[player["Player Name"]]) {
        acc[player["Player Name"]] += goalsCount;
      } else {
        acc[player["Player Name"]] = goalsCount;
      }
      return acc;
    }, {});

  let playerAndTimes = jsonObjPlayer
    .filter((playerObj) => playerObj.Event !== "")
    .reduce((acc, player) => {

      if (acc[player["Player Name"]]) {
        acc[player["Player Name"]] += 1;
      } else {
        acc[player["Player Name"]] = 1;
      }
      return acc;
    }, {});

    //creating probability List
  let playersListProbability = {};
  for (let itr in playersListScores) {
    //Probability Formula applying
    let prob = playersListScores[itr] / playerAndTimes[itr];
    playersListProbability[itr] = prob;
  }

  //Sorting probability list
  let sortable = [];
  for (var score in playersListProbability) {
    sortable.push([score, playersListProbability[score]]);
  }

  sortable.sort(function (a, b) {
    return b[1] - a[1];
  });


  //storing top 10 players list
  let playersWithProb = {};
  for (let i = 0; i < 10; i++) {
    playersWithProb[sortable[i][0]] = sortable[i][1].toFixed(1);
  }

  return playersWithProb;
}

module.exports = top10Players;
