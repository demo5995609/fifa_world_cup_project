// matchesWonPerTeamsmfunction definition
function getMatchesWonPerTeam(matches) {
  let hTeamName = "Home Team Name";
  let aTeamName = "Away Team Name";
  let hTeamGoals = "Home Team Goals";
  let aTeamGoals = "Away Team Goals";


  let matchesPerTeam = matches
    .filter((matche) => matche.City !== "")
    .reduce((acc, match) => {
      if (match[hTeamGoals] > match[aTeamGoals]) {
        if (acc[match[hTeamName]]) {
          acc[match[hTeamName]] += 1;
        } else {
          acc[match[hTeamName]] = 1;
        }
      } else if (match[hTeamGoals] < match[aTeamGoals]) {
        if (acc[match[aTeamName]]) {
          acc[match[aTeamName]] += 1;
        } else {
          acc[match[aTeamName]] = 1;
        }
      } else {
        if (match["Win conditions"] !== "") {
          let conditionString = match["Win conditions"];
          let condition = conditionString.substring(conditionString.length - 7, conditionString.length-2);
     
          let conditionArr = condition.split(" - ");
      
          if (conditionArr[0]>conditionArr[1]) {
            if (acc[match[hTeamName]]) {
              acc[match[hTeamName]] += 1;
            } else {
              acc[match[hTeamName]] = 1;
            }
          } else {
            if (acc[match[aTeamName]]) {
              acc[match[aTeamName]] += 1;
            } else {
              acc[match[aTeamName]] = 1;
            }
          }
        }
      }

      return acc;
    }, {});

  return matchesPerTeam;
}

module.exports = getMatchesWonPerTeam;
