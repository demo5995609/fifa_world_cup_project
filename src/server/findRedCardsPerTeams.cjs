function findRedCardsPerTeams(jsonObjMatches, jsonObjPlayer) {
  let matchIdOfRedTeams = jsonObjPlayer
    .filter((playerObj) => playerObj.Event !== "")
    .reduce((acc, player) => {
      let arrOfEvents = player.Event.split(" ");
      arrOfEvents.map((events) => {
        if (events.startsWith("R")) {
          let teamName = player["Team Initials"];
          let fromPlayerMatchId = player.MatchID;

          acc[fromPlayerMatchId] = teamName;
        }
      });
      return acc;
    }, {});

  let finalResultsOfRedTeams = jsonObjMatches.reduce((acc, match) => {
    let fromMatchesMatchId = match.MatchID;
    let yearOfMatch = match.Year;

    if (
      fromMatchesMatchId !== "" &&
      yearOfMatch == 2014 &&
      matchIdOfRedTeams[fromMatchesMatchId] !== undefined
    ) {
      if (acc[matchIdOfRedTeams[fromMatchesMatchId]]) {
        acc[matchIdOfRedTeams[fromMatchesMatchId]] += 1;
      } else {
        acc[matchIdOfRedTeams[fromMatchesMatchId]] = 1;
      }
    }
    return acc;
  }, {});

  return finalResultsOfRedTeams;
}

module.exports = findRedCardsPerTeams;
